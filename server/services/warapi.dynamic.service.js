const logger = require('../../conf/logger')
const warApiDynamicRepository = require('../repository/warapi.dynamic.repository')
const eventsRepository = require('../repository/event.repository')
const warApiStaticDataService = require('./warapi.static.service')
const warApiClient = require('../warapi/warapi')
const emitSocket = require('../socket/emit.socket')

/**
 * Retrieves all dynamic data from DB and prepare it for the frontEnd
 */
exports.getDynamicData = () => {
  const dynamicDataList = warApiDynamicRepository.list()
  const regionNames = warApiStaticDataService.getRegionNames()
  return dynamicDataList.map((dynamicData) => {
    return {...dynamicData, ...{
        data: JSON.parse(dynamicData.data),
        active: !!regionNames.includes(dynamicData.regionName) // regionNames.includes(dynamicData.regionName) ? true : false thanks webstorm ;)
      }}
  })
}

exports.getEtag = (regionName) => {
  const dynamicData = warApiDynamicRepository.getEtag(regionName)
  if (dynamicData) return dynamicData.result
  return undefined
}

/**
 *
 * @returns {Promise<void>}
 */
exports.updateDynamicData = async () => {
  const regionNames = await warApiStaticDataService.getRegionNames()

  const regionUpdatesPromises = regionNames.map(async regionName => {
    const etag = exports.getEtag(regionName)
    const newDynamicData = await warApiClient.getMapItems(regionName, etag)
    if (!newDynamicData.data) {

    } else {
      const regionId = newDynamicData.data.regionId
      const previousDynamicData = await warApiDynamicRepository.findByRegionId(regionId)

      const previousDynamicDataItems = previousDynamicData && previousDynamicData.data
        ? JSON.parse(previousDynamicData.data).mapItems : []

      previousDynamicDataItems.forEach(mapItem => {
        if (mapItem.iconType > 55 && mapItem.iconType < 59 || mapItem.iconType === 29) {
          const previousMapItem = newDynamicData.data.mapItems.find((storedMapItem) =>
            storedMapItem.x === mapItem.x && storedMapItem.y === mapItem.y)

          if (previousMapItem && previousMapItem !== '' && JSON.stringify(previousMapItem)
            !== JSON.stringify(mapItem)) {

            // discordBot
            // if (mapItem.iconType === 5 || mapItem.iconType === 6 || mapItem.iconType === 7
            //   || mapItem.iconType === 29) {
            //  see old warapi.js line 157
            // }
            if (mapItem.iconType > 55 && mapItem.iconType < 59) {
              eventsRepository.insert(regionId, JSON.stringify(new Date()), JSON.stringify(previousMapItem),
                JSON.stringify(mapItem))
            }
          }
        }
      })
      warApiDynamicRepository.insert(regionName, newDynamicData.etag, regionId,
        JSON.stringify(newDynamicData.data))
    }
  })
  await Promise.all(regionUpdatesPromises)
    .catch(err => logger.error(err))
  await warApiStaticDataService.updateStaticData()
    .catch(err => logger.error(err))
  emitSocket.updateDynamicMap(exports.dynamicMapsData(), eventsRepository.list())
}

/**
 *
 * @returns {*}
 */
exports.dynamicMapsData = () => {
  return warApiDynamicRepository.list()
    .map((dynamicData) => {
      return {
        ...dynamicData,
        ...{
          data: JSON.parse(dynamicData.data),
          active: warApiStaticDataService.getRegionNames().includes(dynamicData.regionName)
        }
      }
    })
}
