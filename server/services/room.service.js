const roomRepository = require('../repository/rooms.repository')

/**
 * A wrapper to apply JSON.stringify if the string is not empty
 * @param data string
 * @param defaultValue any
 */
const parseRoomData = (data, defaultValue) => {
  return !data || data === '' ? defaultValue : JSON.parse(data)
}

/**
 * Returns all data added by users & tied to a specific room
 * @param roomId
 * @returns {{refinery: *, production: *, squads: *, stockpiles: *, logi: *, techtree: *, fobs: *, storage: *, requests: *, arty: *, events: *, misc: *}}
 */
exports.privateRoomData = (roomId) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  // console.log(result)
  const refinery = parseRoomData(room.refinery, {});
  const production = parseRoomData(room.production, {});
  const storage = parseRoomData(room.storage, {});
  const stockpiles = parseRoomData(room.stockpiles, {});

  const techtree = parseRoomData(room.techtree, {});
  const fobs = parseRoomData(room.fobs, {});
  const requests = parseRoomData(room.requests, {});
  const misc = parseRoomData(room.misc, {});

  const arty = parseRoomData(room.arty, {});
  const squads = parseRoomData(room.squads, {});
  const logi = parseRoomData(room.logi, {});
  const events = parseRoomData(room.events, []);
  return {
    refinery, production, storage, stockpiles, techtree, requests, fobs, misc, arty, squads, logi, events,
  };
};

/**
 *
 * @param userId : string
 * @param roomId : string
 * @param rank : string
 * @returns {*}
 */
exports.insertUserRoomRelation = (userId, roomId, rank) => {
  return roomRepository.insertUserRoomRelation(userId, roomId, rank)
}

/**
 *
 * @param roomId : string
 * @returns {{settings, adminname, adminid: (boolean|string|*)}|boolean}
 */
exports.findRoomById = (roomId) => {
  return roomRepository.findRoomById(roomId)
}

/**
 * Create a new room for a user, initialize room settings
 * @param roomId : string
 * @param adminUserId : string
 * @param settings : Object roomSettings
 * @returns {*}
 */
exports.createRoom = (roomId, adminUserId, settings) => {
  const toInsert = {
    id: roomId,
    admin: adminUserId.includes('anonymous') ? 'anonymous' : adminUserId,
    settings: JSON.stringify(settings),
    squads: JSON.stringify({ vehicles: {}, squads: [{ icon: 0, name: 'New Squad', users: [adminUserId] }] })
  }
  const room = roomRepository.createRoom(toInsert)
  exports.insertUserRoomRelation(toInsert.admin, roomId, 1)
  if (adminUserId.includes('anonymous')) {
    exports.insertUserRoomRelation(adminUserId, roomId, 3)
  }
  return room
}

/**
 * Find members of a room
 * @param roomId : string
 */
exports.findUsersInRoom = (roomId) => {
  return roomRepository.findUsersInRoom(roomId)
}

/**
 *
 * @param roomId : string
 * @param techTree : string
 * @returns {*}
 */
exports.updateRoomTech = (roomId, techTree) => {
  return roomRepository.updateRoomTech(roomId, techTree)
}


/**
 *
 * @param roomId : string
 * @param objectType : string
 * @param objectValue : Object
 * @param objectKey : string
 */
exports.updateRoomObject = function (roomId, objectType, objectValue, objectKey) {
  const room = roomRepository.findRoomByIdSimple(roomId)
  // console.log(global);
  if (objectType.includes('misc')) {
    let misc = {};
    if (room.misc !== '') {
      misc = JSON.parse(room.misc)
    }
    const kind = objectType.slice(5)

    if (misc[kind] === undefined) {
      misc[kind] = {}
    }
    misc[kind][objectKey] = objectValue
    misc = JSON.stringify(misc)
    roomRepository.updateRoomMisc(roomId, misc)
  } else {
    let type = {};
    if (room[objectType] !== '') {
      type = JSON.parse(room[objectType])
    }
    type[objectKey] = objectValue
    type = JSON.stringify(type)
    roomRepository.updateRoomObject(roomId, objectType, type)
  }
}

/**
 *
 * @param roomId
 * @param objectType
 * @param objectKey
 */
exports.deleteRoomObject = (roomId, objectType, objectKey) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  if (objectType.includes('misc')) {
    let misc = {};
    if (room.misc !== '') {
      misc = JSON.parse(room.misc)
    }
    const kind = objectType.slice(5)
    if (misc[kind] === undefined) {
      misc[kind] = {}
    }
    delete misc[kind][objectKey]
    misc = JSON.stringify(misc)
    roomRepository.updateRoomMisc(roomId, misc)
  } else {
    let type = {};
    if (room[objectType] !== '') {
      type = JSON.parse(room[objectType])
    }
    delete type[objectKey]
    type = JSON.stringify(type)
    roomRepository.updateRoomObject(roomId, objectType, type)
  }
}

/**
 *
 * @param roomId : string
 * @param stringValue : string
 */
exports.addArtyResult = (roomId, stringValue) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  if (room.arty === '') { room.arty = [] } else {
    room.arty = JSON.parse(room.arty)
  }
  if (room.arty.constructor !== Array) { room.arty = [] }
  room.arty.push(stringValue)
  if (room.arty.length > 5) {
    room.arty.splice(0, 1)
  }
  room.arty = JSON.stringify(room.arty)
  roomRepository.addArtyResult(roomId, room.arty)
}

/**
 *
 * @param roomId : string
 * @param squads : string
 * @param objectType : string
 */
exports.updateRoomSquads = (roomId, squads, objectType) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  if (room.squads === '' || room.squads === '[]') {
    room.squads = {}
  } else {
    room.squads = JSON.parse(room.squads)
  }
  room.squads[objectType] = squads
  room.squads = JSON.stringify(room.squads)
  // console.log("Updating squads",global.squads)
  // let squads = JSON.stringify(packet.squads);
  roomRepository.updateRoomSquads(roomId, room.squads)
}

/**
 *
 * @param roomId : string
 * @param objectType : string
 * @param eventData : string
 * @param eventDate : string
 */
exports.addRoomEvent = (roomId, objectType, eventData, eventDate) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  // discord
  // switch (objectType) {
  //   case 0:// TECH EVENT
  //     discordbot.techEvent(room, eventData);
  //     break;
  // }

  let events = []
  if (room.events !== '') {
    events = JSON.parse(room.events)
  }
  events.push({ type: objectType, date: eventDate, packet: eventData })
  if (events.length > 400) {
    events = events.splice(1)
  }
  events = JSON.stringify(events)
  roomRepository.addRoomEvents(roomId, events)
}

/**
 *
 * @param roomId : string
 * @param timerDate : string
 */
exports.submitOpTimer = (roomId, timerDate) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  let settings = JSON.parse(global.settings);
  settings.optimer = timerDate;
  room.settings = settings;
  // discordbot.startOpTimer(room);
  settings = JSON.stringify(settings);
  roomRepository.addRoomSetings(roomId, settings)
}

/**
 *
 * @param roomId : string
 * @param data : number
 * @returns {*}
 */
exports.toggleRoomSecure = (roomId, data) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  let settings = JSON.parse(room.settings)
  settings.secure = data
  settings = JSON.stringify(settings)
  roomRepository.addRoomSetings(roomId, settings)
  if (data === 1) {
    roomRepository.deleteAnonymousLikeUser(roomId)
    return exports.findUsersInRoom(roomId)
  }
}

/**
 *
 * @param roomId : string
 */
exports.clearRoom = function (roomId) {
  const room = roomRepository.findRoomByIdSimple(roomId)
  const e = {
    id: room.id,
    admin: room.admin,
    settings: room.settings,
  }
  roomRepository.clearRoom(room.id, room.admin, room.settings)
}

/**
 *
 * @param roomId
 */
exports.clearMap = (roomId) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  let misc = {}
  if (room.misc !== '') {
    misc = JSON.parse(room.misc)
    if (misc.rld !== undefined) {
      misc.rld = {}
    }
    if (misc.icon !== undefined) {
      misc.icon = {}
    }
  }
  misc = JSON.stringify(misc)
  roomRepository.clearMap(roomId, misc)
}

/**
 *
 * @param roomId
 * @param type
 * @param data
 */
exports.changeSettings = (roomId, type, data) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  let settings = JSON.parse(room.settings)
  settings[type] = data
  settings = JSON.stringify(settings)
  roomRepository.updateRoomSettings(roomId, settings)
}

/**
 *
 * @param roomId
 * @param settingsType
 */
exports.deleteSettings = (roomId, settingsType) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  let settings = JSON.parse(room.settings);
  // if (settingsType === 'link') {
  //   discordbot.disconnectDiscord(globalid, settings);
  // }
  delete settings[settingsType];
  settings = JSON.stringify(settings);
  roomRepository.updateRoomSettings(roomId, settings)
}

/**
 * Add message to room
 * @param roomId
 * @param packet
 * @param category
 */
exports.addMessage = (roomId, packet, category) => {
  const room = roomRepository.findRoomByIdSimple(roomId)
  let misc = {}
  if (room.misc !== '') {
    misc = JSON.parse(room.misc)
  }
  if (misc.chat === undefined) {
    misc.chat = {}
  }
  if (misc.chat[category] === undefined) {
    misc.chat[category] = []
  }
  misc.chat[category].push(packet)
  misc = JSON.stringify(misc)
  roomRepository.updateRoomMisc(roomId, misc)
}
