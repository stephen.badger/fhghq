const express = require('express')
const path = require('path')
const passport = require('passport');
const shortid = require('shortid');
const session = require('express-session');

const emitSocket = require('../socket/emit.socket')
const cookie = require('../cookies/cookies')
const eventRepository = require('../repository/event.repository')
const warRepository = require('../repository/war.repository')
const userService = require('../services/user.service')
const roomService = require('../services/room.service')
const warApiDynamicDataService = require('../services/warapi.dynamic.service')
const warApiStaticDataService = require('../services/warapi.static.service')
const statsService = require('../services/stats.service')
const { checkAuth } = require('../auth/auth')
const steamAuth = require('../auth/steam')
const noAuth = require('../auth/noauth')

/**
 *
 * @param app : Express
 */
module.exports = (app) => {

  // everything under src/ is served as static files
  app.use(express.static('src'));

  app.use(session({
    secret: 'your secret',
    name: 'name of session id',
    resave: true,
    saveUninitialized: true
  }));

  app.use(passport.initialize());
  app.use(passport.session());

  app.use(express.static(path.resolve('src')));

// steam stuff begin
// /auth/steam redirect to the page the SteamStrategy decides to (real steam login page)
  app.get('/auth/steam',
    passport.authenticate(steamAuth.strategy, { failureRedirect: '/' }),
    function (req, res) {
      res.redirect('/');
    });

  app.get('/auth/steam/return',
    passport.authenticate(steamAuth.strategy, { failureRedirect: '/' }),
    steamAuth.handleReturn)
// steam stuff end

// handle users that just wanna try
  app.post('/noauth', noAuth.handleAuth);


//About
  /**
   * req: http request
   * res: http response
   */
  app.get('/about', checkAuth, (req, res) => {
    res.sendFile(path.resolve('views/about.html'));
  })

  app.get('/', checkAuth, (req, res) => {
    res.sendFile(path.resolve('views/index.html'))
  })

//Get users profile from the DB
  app.post('/getprofile', checkAuth, (req, res) => {
    const id = req.query.id
    const user = userService.getUserById(id)
    const packet = {
      name: user.name,
      blueprint: user.blueprint,
      avatar: user.avatar
    };
    res.send(packet);
  })

//Get info on rooms connected to a profile
  app.post('/getuserrooms', (req, res) => {
    res.send(userService.getUserRooms(req.query.id))
  });

//Leave a room from profile page


  app.post('/leaveroom', checkAuth, (req, res) => {{
    const user = cookie.extractUserFromRequest(req)
    userService.leaveRoom(user.id, req.query.globalid)
    res.redirect('/')
  }})

//Authorization page
  app.get('/auth', (req, res) => {
    res.sendFile(path.resolve('views/auth.html'))
  })


  app.get('/request/:id', checkAuth, (req, res) => {
    res.sendFile(path.resolve('views/request.html'))
  })

  /**
   * ??? access request ?
   */
  app.post('/request2', (req, res) => {
    const roomId = req.query.id
    const coookieUser = cookie.extractUserFromRequest(req)
    const rank = userService.getUserRankInRoom(coookieUser.id, roomId)
    const room = roomService.findRoomById(roomId)

    if (!room) {
      // ? Need details on the ranking system ^^
      res.send({ rank: 8})
    } else {
      const settings = JSON.parse(room.settings);
      res.send({
        rank,
        roomname: settings.name,
        secure: settings.secure,
        admin: room.adminname,
        adminid: room.adminid
      })
    }
  });


  /**
   * SubmitAccessRequest apparently
   */
  app.post('/request3', function (req, res) {
    const roomId = req.query.globalid
    const cookieUser = cookie.extractUserFromRequest(req)
    const rank = userService.getUserRankInRoom(cookieUser.id, roomId)
    if (rank === 7) {
      roomService.insertUserRoomRelation(cookieUser.id, roomId, 5)
      const user = userService.getRequester(cookieUser.id)
      emitSocket.accessRequest(roomId, user)
    }
    res.redirect('/');
  });

  /**
   * Request room not "secured" (created without Steam log-in and 'secure' enabled)
   */
  app.post('/requestPassword',  function (req, res) {
    const roomId = req.query.globalid
    const cookieUser = cookie.extractUserFromRequest(req)
    const room = roomService.findRoomById(roomId)
    const settings = JSON.parse(room.settings)
    if (settings.password === req.query.password) {
      roomService.insertUserRoomRelation(cookieUser.id, roomId, 3)
      res.send('right')
    } else {
      res.send('wrong')
    }
  });

  /**
   * Creates a room
   */
  app.post('/createroom', (req, res) => {
    var id = shortid.generate();
    var userId = req.query.id;
    let settings = req.query.settings;
    roomService.createRoom(id, userId, settings)
    res.send(id);
  });

//Pulls room from a unique link
  app.get('/room/:id', checkAuth, (req, res) => {
    const roomId = req.params.id
    const cookieUser = cookie.extractUserFromRequest(req);
    const rank = userService.getUserRankInRoom(cookieUser.id, roomId)
    if (rank < 4) {
      res.sendFile(path.resolve('views/global.html'));
    } else {
      res.redirect('/request/' + req.params.id);
    }
  });


  /**
   * Original comment: "pulls room from a unique link, part 2"
   * ? Not sure what it does
   * Looks like it pulls all the data
   */
  app.post('/getroom', function (request, response) {
    const roomId = request.query.id
    const data = {}
    data.users = roomService.findUsersInRoom(roomId)
    data.meta = roomService.findRoomById(roomId)
    data.meta.settings = JSON.parse(data.meta.settings)
    data.dynamic = warApiDynamicDataService.getDynamicData()
    data.static = warApiStaticDataService.list()
    data.private = roomService.privateRoomData(roomId)
    data.events = { events: eventRepository.list() }
    data.stats = { totalplayers: statsService.totalPlayers }

    response.send(data);
  });

  app.get('/getcurrentwar', function (request, response) {
    var packet = {
      totalplayers: statsService.totalPlayers,
      warstats: statsService.warStats,
      wr: statsService.warReport,
      currentwar: undefined
    };
    response.send(packet)
  });

//Pulls room from a unique link
  app.get('/getwar/:warnumber', function (request, response) {
    return warRepository.getWarById(request.params.warnumber)
  });

}
