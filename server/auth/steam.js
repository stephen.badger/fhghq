const passport = require('passport')
const SteamStrategy = require('passport-steam').Strategy
const express = require('express')
const shortid = require('shortid')

const conf = require('../../conf/config')
const logger = require('../../conf/logger')
const userService = require('../services/user.service')

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

passport.use(new SteamStrategy({
    returnURL: conf.fhghq.url + '/auth/steam/return',
    realm: conf.fhghq.url,
    apiKey: conf.steamApi.key
  },
  function (identifier, profile, done) {
    // asynchronous verification, for effect...
    process.nextTick(function () {
      profile.identifier = identifier;
      return done(null, profile);
    });
  }
));

exports.strategy = 'steam'

/**
 * Steam Authentication is an OAUTH2 authentication.
 * That means
 *   1. steam authenticates the user on its side, and then only
 *   2. returns us some information about the logged in user.
 * @param req the http request made by the user
 * @param res the http response the user will get
 */
exports.handleReturn = (req, res) => {

  let user = userService.getUserById(req.user._json.steamid)

  // Create or Update user
  if (user) {
    // Update its current pseudonym & avatar
    user = {...user, ...{
        name:  req.user._json.personaname,
        avatar: req.user._json.avatarmedium
      }}
    logger.info(`Steam user ${user.id} already exists, updating name && avatar`)
    userService.updateUser(user)
  } else {
    logger.info(`Steam user ${req.user._json.steamid} does not exists, creating..`)
    user = {
      id: req.user._json.steamid,
      salt: shortid.generate(),
      name: req.user._json.personaname,
      avatar: req.user._json.avatarmedium
    }
    userService.createUser(user)
  }

  // Set user info cookie
  res.cookie('steamid', user.id);
  res.cookie('salt', user.salt);
  if (!req.headers['cookie'] || !req.headers['cookie'].includes('redir_room')) {
    res.redirect('/');
  } else {
    const cookiestring = req.headers['cookie'];
    const id = cookiestring.substring(cookiestring.indexOf(' redir_room') + 12, cookiestring.indexOf(' redir_room') + 21);
    res.redirect('/room/' + id);
  }
}
