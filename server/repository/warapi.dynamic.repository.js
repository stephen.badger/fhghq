/**
 * War API Dynamic Data repository
 */
const sqlClient = require('./client')()

/**
 * Look for all events in the db
 * Limited to the 400 latests events
 * @returns {*}
 */
exports.list = () => {
  return sqlClient.prepare('SELECT * FROM apidata_dynamic').all()
}

/**
 *
 * @returns {*}
 */
exports.createTable = () => {
  return sqlClient.prepare('CREATE TABLE apidata_dynamic (regionName TEXT PRIMARY KEY, regionId INT, data TEXT, etag TEXT);')
    .run()
}

/**
 * Returns dynamic data for a regionId
 * @param regionId : string
 */
exports.findByRegionId = (regionId) => {
  return sqlClient.prepare('SELECT * FROM apidata_dynamic WHERE regionId = ?;')
    .get(regionId)
}


/**
 *
 * @param regionName : string
 * @param etag : string
 * @param regionId : string
 * @param data : string
 * @returns {*}
 */
exports.insert = (regionName, etag, regionId, data) => {
  return sqlClient.prepare('INSERT OR REPLACE INTO apidata_dynamic (regionName, regionId, data, etag) VALUES (@regionName, @regionId, @data, @etag);')
    .run({
      regionName,
      etag,
      regionId,
      data
    })
}

exports.getEtags = () => {
  return sqlClient.prepare('SELECT regionName, etag FROM apidata_dynamic;')
    .all()
}


/**
 * @param regionName : string
 * @returns {*}
 */
exports.getEtag = (regionName) => {
  return sqlClient.prepare('SELECT regionName, etag FROM apidata_dynamic WHERE regionName = ?;')
    .get(regionName)
}

