
const logger = require('../../conf/logger')
const SQLite = require('./client')()

////SQL MANAGEMENT

const create_global_table = SQLite.prepare("CREATE TABLE IF NOT EXISTS global (id TEXT PRIMARY KEY, admin TEXT, settings TEXT, techtree TEXT, fobs TEXT, requests TEXT, misc TEXT, arty TEXT, squads TEXT, refinery TEXT, production TEXT, storage TEXT, stockpiles TEXT, logi TEXT, events TEXT);");
const create_users_table = SQLite.prepare("CREATE TABLE IF NOT EXISTS users (id TEXT PRIMARY KEY, salt TEXT, name TEXT, avatar TEXT);");
// //SETTINGS: name, side, channel, secure, password
const create_userGlobal_table = SQLite.prepare("CREATE TABLE IF NOT EXISTS userglobal (id TEXT PRIMARY KEY, userid TEXT, globalid TEXT, rank INT, role INT, FOREIGN KEY (userid) REFERENCES users(id), FOREIGN KEY (globalid) REFERENCES global(id));");
const create_events = SQLite.prepare("CREATE TABLE IF NOT EXISTS events (region INT, date TEXT, prevItem TEXT, newItem TEXT);")
const create_apidata_dynamic_table = SQLite.prepare('CREATE TABLE IF NOT EXISTS apidata_dynamic (regionName TEXT PRIMARY KEY, regionId INT, data TEXT, etag TEXT);')
const create_apidata_static_table = SQLite.prepare('CREATE TABLE IF NOT EXISTS apidata_static (regionName TEXT PRIMARY KEY, regionId INT, data TEXT, etag TEXT);')

logger.info('Creating tables')
create_global_table.run()
create_users_table.run()
create_userGlobal_table.run()
create_events.run()
create_apidata_dynamic_table.run()
create_apidata_static_table.run()
logger.info('Tables created')

try {
  const insert_anonymous = SQLite.prepare("INSERT INTO users (id, salt, name, avatar) VALUES ('anonymous','anonymous','anonymous','anonymous')")
  insert_anonymous.run()
} catch (error) {
  if (error.code === 'SQLITE_CONSTRAINT_PRIMARYKEY') {
    logger.info('Anonymous user already created')
  } else {
    logger.error(`Could not create anonymous user: ${error}`)
  }
}

//sql.prepare("CREATE TABLE war history (warnumber INT, warstats TEXT, events TEXT, reports TEXT, startpoint TEXT);").run();
exports.wipe = function (){
  SQLite.prepare("DELETE FROM userglobal;").run();
  SQLite.prepare("DELETE FROM global;").run();
  //sql.prepare("DELETE FROM towns;").run();
  //sql.prepare("DELETE FROM forts;").run();
  //sql.prepare("DELETE FROM fobs;").run();
  //sql.prepare("DELETE FROM ambushes;").run();
  //sql.prepare("DELETE FROM requests;").run();
  //sql.prepare("DELETE FROM techtrees;").run();
  //sql.prepare("DELETE FROM mines;").run();
}



exports.cunt = function test(string){
  console.log("One-time command module " + string);
}

