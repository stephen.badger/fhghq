const fetch = require('node-fetch')

const logger = require('../../conf/logger')
const conf = require('../../conf/config')
const api_url = conf.warApi.liveUrl
const warApi = new URL('/api/worldconquest/war/', api_url).toString()
const mapsApi = new URL('/api/worldconquest/maps/', api_url).toString()
const warReportApi = new URL('/api/worldconquest/warReport/', api_url).toString()

const headers = (etag) => {
  if (etag) {
    return {
      'If-None-Match': etag
    }
  }
  return {}
}

exports.getMaps = async () => {
  return await fetch(mapsApi, { method: 'get', cache: 'no-cache'})
    .then(response => {
      logger.info(`Got map list from ${mapsApi}`)
      return response.json()
    })
    .catch(reason => logger.error(`Could not fetch map list from ${mapsApi}: ${reason}`))
}

/**
 *
 * @param mapId
 * @param etag last etag
 * @returns {Promise<{data: { mapItems: []}, etag: string}|*>}
 */
exports.getMapItems = async (mapId, etag) => {
  const mapItemsApi = new URL(`${mapId}/dynamic/public`, mapsApi).toString()
  return await fetch(mapItemsApi, {
      method: 'get',
      headers: headers(etag),
      cache: 'no-cache'
    })
    .then(async response => {
      if (response.status === 304) {
        logger.info(`No new dynamic data from ${mapItemsApi}`)
        return { etag: response.headers.get('etag') }
      } else {
        logger.info(`Got dynamic data from ${mapItemsApi}`)
        return { data: await response.json(), etag: response.headers.get('etag') }
      }
    })
    .catch(reason => logger.error(`Could not fetch dynamic from ${mapItemsApi}: ${reason}`))
}

/**
 * Get static data for a region
 * @param mapId : string
 * @param etag
 * @returns {Promise<Response|*>}
 */
exports.getMapTextItems = async (mapId, etag) => {
  const mapTextItemsApi = new URL(`${mapId}/static`, mapsApi).toString()
  return await fetch(mapTextItemsApi, {
      method: 'get',
      headers: headers(etag),
      cache: 'no-cache'
    })
    .then(async response => {
      if (response.status === 304) {
        logger.info(`No new static data from ${mapTextItemsApi}`)
        return { etag: response.headers.get('etag') }
      } else {
        logger.info(`Got static data from ${mapTextItemsApi}`)
        return { data: await response.json(), etag: response.headers.get('etag') }
      }
    })
    .catch(reason => logger.error(`Could not fetch static from ${mapTextItemsApi}: ${reason}`))
}

/**
 * Get overall war statistics
 * @returns {Promise<Response|*>}
 */
exports.getWarStats = async () => {
  return await fetch(warApi, { method: 'get', cache: 'no-cache'})
    .then(response => {
      logger.info(`Got war stats from ${warApi}`)
      return response.json()
    })
    .catch(reason => logger.error(`Could not fetch war stats from ${warApi}: ${reason}`))
}

/**
 * Get statistics for a region
 * @param mapId : string
 * @returns {Promise<Response|*>}
 */
exports.getMapStats = async (mapId) => {
  const mapStatsApi = new URL(`${mapId}`, warReportApi).toString()
  return await fetch(mapStatsApi, { method: 'get', cache: 'no-cache'})
    .then(response => {
      logger.info(`Got map stats from ${mapStatsApi}`)
      return response.json()
    })
    .catch(reason => logger.error(`Could not fetch map stats from ${mapStatsApi}: ${reason}`))
}

