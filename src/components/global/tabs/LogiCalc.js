import React from 'react';
import cost from '../../../_static/cost';
import clone from 'clone';

import fullItems from '../../../_static/items';

const materials = fullItems.filter(item => item.type === 'material' || item.type === 'time')
const items = fullItems.filter(item => item.refinery || item.factory || item.massProductionFactory);

export class LogiCalc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: 0,
      category: 0,
      orders: {},
      array: []
    };
    this.SetFilter = this.SetFilter.bind(this);
    this.AddItem = this.AddItem.bind(this);
    this.ChangeItem = this.ChangeItem.bind(this);
    this.RemoveItem = this.RemoveItem.bind(this);
  }

  /**
   *
   * @param item {{name: string, crates: number}[]}
   * @constructor
   */
  AddItem(item) {
    this.setState(state => {
      const orders = clone(state.orders)
      if (item) {
        if (item.crate) {
          orders[item.name] = {
            crates:  (orders[item.name] && orders[item.name].crates) ? orders[item.name].crates + 1 : 1, // Add 1 item to existing or set to 1,
            item
          }
        } else if (item.bigCrate) {
          orders[item.name] = {
            bigCrates:  (item.bigCrate && orders[item.name] && orders[item.name].bigCrates) ? orders[item.name].bigCrates + 1 : 1, // Add 1 item to existing or set to 1,
            item
          }
        }
      }
      return { orders }
    });
  }

  ChangeItem(index, event) {
    let array = this.state.array;
    let value = event.target.value;
    if (value <= 10000) {
      let item = array[index];
      if (value === '') {
        value = 0;
      }
      item.crates = value;
      item.amount = cost.cost[item.catid][item.itemid].i * item.crates;
      this.setState({ array: array });
    }
  }

  RemoveItem(index) {
    this.setState(state => {
      let array = JSON.parse(JSON.stringify(state.array));
      array.splice(index, 1);
      return {
        array: array
      };
    });
  }

  SetFilter(filter) {
    this.setState({
      filter: filter
    });
  }

  GetButton(cat, id) {
    return (
      <button
        className="requestmodal_itembtn"
        onClick={() => this.AddItem(cat, id)}
        key={'logicalc' + cat + '|' + id}
      >
        <img className="requestmodal_itemimg" src={cost.cost[cat][id].src}/>
      </button>
    );
  }

  GetTotal(type) {
    let amount = 0;
    this.state.array.forEach(k => {
      if (cost.cost[k.catid][k.itemid][type] != undefined) {
        amount += cost.cost[k.catid][k.itemid][type] * k.crates;
      }
    });
    return <a>{amount}</a>;
  }

  GetCrates() {
    let crates = 0;
    let array = this.state.array;
    for (let i = 0; i < array.length; i++) {
      if (array[i].catid < 7) {
        crates += Number(array[i].crates);
      }
    }
    return crates;
  }

  GetVehicles() {
    let crates = 0;
    this.state.array.forEach(k => {
      if (k.catid === 7) {
        crates += Number(k.crates);
      }
    });
    return crates;
  }

  GetButtons() {
    return items.map((item, index) => <button
        className="requestmodal_itembtn"
        onClick={() => this.AddItem(item)}
        key={`logicalc-item-${index}`}>
        <img title={item.displayName} className="requestmodal_itemimg" src={`/_static/${item.icon}`} alt={item.displayName}/>
      </button>)
  }

  /**
   * Show total cost row
   */
  totalCostRow(title, orders) {

  }

  shipyardOrderCost(order) {

  }

  /**
   *
   * @param order
   */
  orderCost(order) {
    const orderQuantity = order.crates ? order.crates : order.bigCrates
    const orderCost = order.item.cost

    const materialOrderCost = {}

    Object.keys(orderMaterial => {
      materialOrderCost[orderMaterial] = !!materialOrderCost[orderMaterial] ?
        materialOrderCost[orderMaterial] + orderCost[orderMaterial] : orderCost[orderMaterial]
    })

    return {
      ...{
        crates: order.item.crate ? orderQuantity * order.item.crate : 0,
        bigCrates: order.item.bigCrate ? orderQuantity * order.item.bigCrate : 0,
      },
      ...materialOrderCost
    }
  }

  costTable() {
    const cost = {
      bigCrates: 0,
      factory: {
        crates: 0
      },
      massProductionFactory: {},
      shipyard: {}
    }

    Object.values(this.state.orders).map((order, index) => {
      const orderItem = order.item
      // compute factory cost
      if (orderItem.factory) {

        //compute crates
        cost.factory.crates = !!cost.factory.crates ? cost.factory.crates + order.crates : order.crates
        // compute cost
        Object.keys(orderItem.cost).forEach((material, index) => {
          const materialCost =  orderItem.cost[material] * order.crates

          // if (material !== 'time') {
            // if eligible to mass production factory
            if (cost.factory.crates >= 3 && orderItem.massProductionFactory) {
              // compute mass factory cost
            }
            cost.factory[material] = !!cost.factory[material] ? cost.factory[material] + materialCost : materialCost
          // }
        })
      }

      if (orderItem.shipyard) {
        Object.keys(orderItem.cost).forEach((material, index) => {
          const materialCost =  orderItem.cost[material] * order.bigCrates
          cost.shipyard[material] = !!cost.shipyard[material] ? cost.shipyard[material] + materialCost : materialCost
        })
      }

      if (orderItem.massProductionFactory && (order.crates >= 3 || order.bigCrates >= 3)) {
        cost.massProductionFactory.bigCrates = !!cost.massProductionFactory.bigCrates
          ? cost.massProductionFactory.bigCrates + order.bigCrates : order.bigCrates
        let discount = 90
        for (const crateIndex of Array(order.bigCrates).keys()) {
          if (discount > 50) discount = discount - (crateIndex * 10)

          Object.keys(orderItem.cost).forEach((material, index) => {
            const materialCost =  (orderItem.cost[material] * discount / 100)
            cost.massProductionFactory[material] = !!cost.massProductionFactory[material]
              ? cost.massProductionFactory[material]  + materialCost : materialCost
          })
        }
      }
    })

    return  <h4 className="totalh">
      Total cost:
      <h4 className="totalh" style={{display: 'flex'}}>
        Factory:
        {Object.keys(cost.factory)
          .filter(key => key !== 'crates' && key !== 'bigCrates')
          .map((materialName, index) => {
          const requiredMaterialItem = materials.find(rawMatItem => rawMatItem.name === materialName)
          return <p key={requiredMaterialItem.name + index}>
            <img
              className="totalicon"
              src={`/_static/${requiredMaterialItem.icon}`}
            />
            <a>{cost.factory[materialName]}</a>
          </p>
        })}
        <img
          className="totalicon"
          src="https://cdn.glitch.com/98ac14b2-4603-4541-b92e-320b855d2e65%2FTruckVehicleIcon.png?1542349073226"
        />
        <a id="totalvehicles">{this.GetVehicles()}</a>
      </h4>

      <h4 className="totalh" style={{display: 'flex'}}>
        Shipyard:
        {Object.keys(cost.shipyard)
          .filter(key => key !== 'crates' && key !== 'bigCrates')
          .map((materialName, index) => {
          const requiredMaterialItem = materials.find(rawMatItem => rawMatItem.name === materialName)
          return <p key={requiredMaterialItem.name + index}>
            <img
              className="totalicon"
              src={`/_static/${requiredMaterialItem.icon}`}
            />
            <a>{cost.shipyard[materialName]}</a>
          </p>
        })}
      </h4>
      <h4 className="totalh" style={{display: 'flex'}}>
        Mass Production Factory:
        {Object.keys(cost.massProductionFactory)
          .filter(key => key !== 'crates' && key !== 'bigCrates')
          .map((materialName, index) => {
          console.log('material ' )
          const requiredMaterialItem = materials.find(rawMatItem => rawMatItem.name === materialName)
          return <p key={requiredMaterialItem.name + index}>
            <img
              className="totalicon"
              src={`/_static/${requiredMaterialItem.icon}`}
            />
            <a>{cost.massProductionFactory[materialName]}</a>
          </p>
        })}
      </h4>
    </h4>
  }

  render() {
    return (
      <React.Fragment>
        <p>Orders Calculator</p>
        <div className="row">
          <div className="col-12 col-sm-12 col-md-5">
            <div className="categoryrow">
              <cost.filterrow
                filter={this.state.filter}
                setfilter={this.SetFilter}
              />
            </div>
            <br/>
            <div id="category">{this.GetButtons()}</div>
          </div>

          <div className="col-md-7">
            <div>
              <form>
                {this.costTable()}
              </form>
            </div>

            <div id="itemlist" className="container indextable">
              <table
                className="table table-condensed indextable"
                id="itemtable"
              >
                <colgroup>
                  <col width="40" className="tablecol"/>
                  <col width="60" className="tablecol"/>
                  <col/>
                  <col width="60" className="tablecolwide"/>
                  <col width="60" className="tablecol"/>
                  <col width="60" className="tablecol"/>
                  <col width="60" className="tablecol"/>
                  <col width="60" className="tablecol"/>
                  <col width="60" className="tablecol"/>
                </colgroup>
                <thead className="indextable darkheader">
                <tr>
                  <th className="tablecol"></th>
                  <th></th>
                  <th>
                    <p>Item</p>
                  </th>
                  <th>
                    <img
                      className="crate"
                      src="https://cdn.glitch.com/6393f3fd-16a7-4641-ae3d-994f8e7cea4e%2FCrateItemIcon.png?1548192460894"
                      style={{
                        width: 34,
                        height: 34
                      }}
                    />
                  </th>
                  <th>
                    <p>#</p>
                  </th>
                  <th>
                    <img
                      className="tabletotalicon"
                      src={cost.cost[0][0].src}
                    />
                  </th>
                  <th>
                    <img
                      className="tabletotalicon"
                      src={cost.cost[0][1].src}
                    />{' '}
                  </th>
                  <th>
                    <img
                      className="tabletotalicon"
                      src={cost.cost[0][2].src}
                    />
                  </th>
                  <th>
                    <img
                      className="tabletotalicon"
                      src={cost.cost[0][8].src}
                    />
                  </th>
                </tr>
                </thead>
                <tbody>
                {this.state.array.map((obj, index) => (
                  <Item key={index} index={index} obj={obj} parent={this}/>
                ))}
                </tbody>
              </table>
            </div>
          </div>
          <div id="logi_image_container">
            <img
              id="logi_image"
              src="https://cdn.glitch.com/84b19724-a86b-4caa-8e69-1e9c973e043f%2Ftabl-min.jpg?v=1568729797701"
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

///////////////////////////////////
function Item(props) {
  //console.log("Item props",props)
  function GetCost(type) {
    if (cost.cost[props.obj.catid][props.obj.itemid][type] == undefined) {
      return null;
    } else {
      return (
        <p>{cost.cost[props.obj.catid][props.obj.itemid][type] * props.obj.crates}</p>
      );
    }
  }

  return (
    <tr>
      <td>
        <button
          type="button"
          className="removebutton"
          onClick={() => props.parent.RemoveItem(props.index)}
        >
          <img
            className="removebutton"
            src="https://cdn.glitch.com/dd3f06b2-b7d4-4ccc-8675-05897efc4bb5%2FX.png?v=1557668374293"
          />
        </button>
      </td>
      <td>
        <img
          className="requestmodal_itemimg"
          src={cost.cost[props.obj.catid][props.obj.itemid].src}
        />
      </td>
      <td>
        <p>{cost.cost[props.obj.catid][props.obj.itemid].name}</p>
      </td>
      <td>
        <input
          type="number"
          min={0}
          value={props.obj.crates}
          style={{ width: 60 }}
          onChange={event => props.parent.ChangeItem(props.index, event)}
        ></input>
      </td>
      <td>
        <p>{props.obj.amount}</p>
      </td>
      <td>
        {GetCost('b')}
      </td>
      <td>
        {GetCost('r')}
      </td>
      <td>
        {GetCost('e')}
      </td>
      <td>
        {GetCost('he')}
      </td>
    </tr>
  );
}
