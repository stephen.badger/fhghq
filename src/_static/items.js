/**
 *
 * @type {({displayName: string, name: string, icon: string, type: string}|{displayName: string, name: string, icon: string, type: string}|{displayName: string, name: string, icon: string, type: string}|{displayName: string, name: string, icon: string, type: string}|{displayName: string, name: string, icon: string, type: string})[]}
 */
module.exports =  [
  /**
   * Resources
   */
  {
    type: 'time',
    name: 'time',
    displayName: 'Time (s)',
    icon: 'icons/Menus/IconConditionTime.png'
  },
  {
    type: 'resource',
    name: 'aluminum',
    displayName: 'Aluminum',
    icon: 'icons/ItemIcons/ResourceAluminumIcon.png'
  },
  {
    type: 'resource',
    name: 'iron',
    displayName: 'Iron',
    icon: 'icons/ItemIcons/ResourceIronIcon.png'
  },
  {
    type: 'resource',
    name: 'salvage',
    displayName: 'Salvage',
    icon: 'icons/ItemIcons/SalvageIcon.png'
  },
  {
    type: 'resource',
    name: 'crudeOil',
    displayName: 'Crude Oil',
    icon: 'icons/ItemIcons/CrudOilIcon.png'
  },
  {
    type: 'resource',
    name: 'component',
    displayName: 'Component',
    icon: 'icons/ItemIcons/ComponentsIcon.png'
  },
  {
    type: 'resource',
    name: 'sulfur',
    displayName: 'Sulfur',
    icon: 'icons/ItemIcons/SulfurIcon.png'
  },
  /**
   * Materials
   */
  {
    displayName: 'Basic Material',
    name: 'basicMaterial',
    type: 'material',
    icon: 'icons/ItemIcons/BasicMaterialsIcon.png',
    crate: 100,
    refinery: true,
    cost: {
      salvage: 2,
      timeFor100Unit: 60
    }
  },
  {
    displayName: 'Refined Material',
    name: 'refinedMaterial',
    type: 'material',
    icon: 'icons/ItemIcons/RefinedMaterialsIcon.png',
    refinery: true,
    cost: {
      component: 20,
      timeFor100Unit: 300
    }
  },
  {
    displayName: 'Explosive Material',
    name: 'explosiveMaterial',
    type: 'material',
    icon: 'icons/ItemIcons/ExplosiveMaterialIcon.png',
    refinery: true,
    cost: {
      salvage: 10,
      timeFor100Unit: 240
    }
  },
  {
    displayName: 'Diesel',
    name: 'diesel',
    type: 'material',
    icon: 'icons/ItemIcons/Diesel.png',
    refinery: true,
    cost: {
      salvage: 10,
      timeFor100Unit: 600
    }
  },
  {
    displayName: 'Petrol',
    name: 'petrol',
    type: 'material',
    icon: 'icons/ItemIcons/RefinedFuelIcon.png',
    refinery: true,
    cost: {
      crudeOil: 3,
      timeFor100Unit: 360
    }
  },
  {
    displayName: 'Heavy Explosive Material',
    name: 'heavyExplosiveMaterial',
    type: 'material',
    icon: 'icons/ItemIcons/HeavyExplosiveMaterialIcon.png',
    refinery: true,
    cost: {
      explosiveMaterial: 20
    }
  },
  {
    name: 'concreteMaterial',
    displayName: 'Concrete Material',
    type: 'material',
    concreteMixer: true,
    icon: 'icons/ItemIcons/CivicMaterialsIcon.png'
  },
  {
    name: 'aluminumAlloy',
    displayName: 'Aluminum Alloy',
    type: 'material',
    icon: 'icons/ItemIcons/ResouceAluminumRefinedIcon.png',
    refinery: true,
    cost: {
      aluminum: 1
    }
  },
  {
    name: 'ironAlloy',
    displayName: 'Iron Alloy',
    type: 'material',
    icon: 'icons/ItemIcons/ResouceIronRefinedIcon.png',
    refinery: true,
    cost: {
      iron: 1
    }
  },
  /**
   * Firearms
   */
  {
    name: 'cometaT29',
    displayName: 'Cometa T2-9',
    icon: 'icons/ItemIcons/RevolverItemIcon.png',
    type: 'smallArms',
    crate: 20,
    ammo: ['dot44'],
    massProductionFactory: true,
    factory: true,
    cost: {
      basicMaterial: 60,
      time: 50
    }
  },
  {
    name: 'dot44',
    displayName: '.44',
    damageType: 'lightKinetic',
    icon: 'icons/ItemIcons/RevolverAmmoItemIcon.png',
    type: 'smallArms',
    crate: 40,
    massProductionFactory: true,
    factory: true,
    cost: {
      basicMaterial: 40,
      time: 40
    }
  },
  {
    name: 'number2LoughCaster',
    displayName: 'No. 2 Loughcaster',
    icon: 'icons/ItemIcons/RifleItemIcon.png',
    type: 'smallArms',
    crate: 20,
    massProductionFactory: true,
    team: 'warden',
    ammo: ['7dot62mm'],
    factory: true,
    cost: {
      basicMaterial: 100,
      time: 70
    }
  },
  {
    name: 'blakerow871',
    displayName: 'Blakerow 871',
    icon: 'icons/ItemIcons/CarbineItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 20,
    team: 'warden',
    ammo: ['7dot62mm'],
    factory: true,
    cost: {
      basicMaterial: 140,
      time: 80
    }
  },
  {
    name: '7dot62mm',
    displayName: '7.62 mm',
    damageType: 'lightKinetic',
    icon: 'icons/ItemIcons/RifleAmmoItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 40,
    team: 'warden',
    factory: true,
    cost: {
      basicMaterial: 80,
      time: 50
    }
  },
  {
    name: 'fiddlerSubmachineGunModel868',
    displayName: 'Fiddler Submachine Gun Model 868',
    icon: 'icons/ItemIcons/SubMachineGunIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 20,
    team: 'warden',
    ammo: ['9mmSMG'],
    factory: true,
    cost: {
      basicMaterial: 120,
      time: 80
    }
  },
  {
    name: '9mmSMG',
    displayName: '9mm SMG',
    damageType: 'lightKinetic',
    icon: 'icons/ItemIcons/SubMachineGunAmmoIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 40,
    team: 'warden',
    factory: true,
    cost: {
      basicMaterial: 80,
      time: 50
    }
  },
  {
    name: 'brasaShotgun',
    displayName: 'Brasa Shotgun',
    icon: 'icons/ItemIcons/ShotgunItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 20,
    ammo: ['buckshot'],
    factory: true,
    cost: {
      basicMaterial: 120,
      time: 80
    }
  },
  {
    name: 'buckshot',
    displayName: 'Buckshot',
    icon: 'icons/ItemIcons/ShotgunAmmoItemIcon.png',
    type: 'smallArms',
    crate: 40,
    factory: true,
    cost: {
      basicMaterial: 80,
      time: 50
    }
  },
  {
    name: 'aaltoStormRifle24',
    displayName: 'Aalto Storm Rifle 24',
    icon: 'icons/ItemIcons/AssaultRifleItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 10,
    ammo: ['7dot92mm'],
    team: 'warden',
    factory: true,
    cost: {
      basicMaterial: 165,
      time: 50
    }
  },
  {
    name: '7dot92mm',
    displayName: '7.92 mm',
    damageType: 'lightKinetic',
    icon: 'icons/ItemIcons/AssaultRifleAmmoItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 30,
    factory: true,
    cost: {
      basicMaterial: 120,
      time: 60
    }
  },
  {
    name: 'clancyRacaM4',
    displayName: 'Clancy-Raca M4',
    icon: 'icons/ItemIcons/SniperRifleItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    team: 'warden',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 200,
      refinedMaterial: 15
    }
  },
  {
    name: '8dot5mm',
    displayName: '8.5 mm',
    icon: 'icons/ItemIcons/SniperRifleAmmoItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    damageType: 'lightKinetic',
    crate: 10,
    factory: true,
    cost: {
      basicMaterial: 150,
      time: 100
    }
  },
  {
    name: 'cascadier873',
    displayName: 'Cascadier 873',
    icon: 'icons/ItemIcons/PistolLightWItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    crate: 20,
    team: 'warden',
    ammo: ['8mm'],
    factory: true,
    cost: {
      basicMaterial: 60,
      time: 50
    }
  },
  {
    name: '8mm',
    displayName: '8mm',
    icon: 'icons/ItemIcons/PistolAmmoItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    damageType: 'lightKinetic',
    crate: 40,
    factory: true,
    cost: {
      basicMaterial: 40,
      time: 20
    }
  },
  /**
   * heavyArms
   */
  {
    name: '12dot7mm',
    displayName: '12.7mm',
    icon: 'icons/ItemIcons/MachineGunAmmoIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'heavyKinetic',
    crate: 20,
    factory: true,
    cost: {
      basicMaterial: 100,
      time: 70
    }
  },

  // mortar
  {
    name: 'cremariMortar',
    displayName: 'Cremari Mortar',
    icon: 'icons/ItemIcons/MortarItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    ammo: ['mortarShell', 'mortarShellShrapnel', 'mortarShellFlare'],
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 100,
      refinedMaterial: 25,
    }
  },
  {
    name: 'mortarShell',
    displayName: 'Mortar Shell',
    icon: 'icons/ItemIcons/MortarAmmoIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'highExplosive',
    crate: 15,
    factory: true,
    cost: {
      basicMaterial: 60,
      explosiveMaterial: 35,
      time: 112
    }
  },
  {
    name: 'mortarShellShrapnel',
    displayName: 'Mortar Shrapnel Shell',
    icon: 'icons/ItemIcons/MortarAmmoIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'shrapnel',
    crate: 15,
    factory: true,
    cost: {
      basicMaterial: 60,
      explosiveMaterial: 15,
      time: 112
    }
  },
  {
    name: 'mortarShellFlare',
    displayName: 'Mortar Flare Shell',
    icon: 'icons/ItemIcons/MortarAmmoIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'flare',
    crate: 15,
    factory: true,
    cost: {
      basicMaterial: 60,
      explosiveMaterial: 10,
      time: 112
    }
  },
  {
    name: 'greenAshGrenade',
    displayName: 'Green Ash Grenade',
    icon: 'icons/ItemIcons/DeadlyGas01Icon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'poisonousGas',
    crate: 20,
    factory: true,
    cost: {
      basicMaterial: 140,
      time: 100
    }
  },
  {
    name: 'cutlerLauncher4',
    displayName: 'Cutler Launcher 4',
    icon: 'icons/ItemIcons/RpgItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 5,
    team: 'warden',
    damageType: 'explosive',
    ammo: ['rpgShell'],
    factory: true,
    cost: {
      basicMaterial: 100,
      refinedMaterial: 25,
      time: 100
    }
  },
  {
    name: 'rpgShell',
    displayName: 'R.P.G. Shell',
    icon: 'icons/ItemIcons/RpgAmmoItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 15,
    damageType: 'explosive',
    factory: true,
    cost: {
      basicMaterial: 60,
      explosiveMaterial: 75,
      time: 112
    }
  },
  {
    name: 'antiTankStickyBomb',
    displayName: 'Anti-Tank Sticky Bomb',
    icon: 'icons/ItemIcons/StickyBombIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 10,
    damageType: 'antiTankExplosive',
    factory: true,
    cost: {
      basicMaterial: 50,
      explosiveMaterial: 50,
      time: 75
    }
  },
  {
    name: 'mammon91b',
    displayName: 'Mammon 91-b',
    icon: 'icons/ItemIcons/HEGrenadeItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 25,
    damageType: 'explosive',
    factory: true,
    cost: {
      basicMaterial: 100,
      explosiveMaterial: 10,
      time: 80
    }
  },
  {
    name: '20NevilleAntiTankRifle',
    displayName: '20 Neville Anti-Tank Rifle',
    icon: 'icons/ItemIcons/ATRifleItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    ammo: ['20mm'],
    crate: 5,
    team: 'warden',
    factory: true,
    cost: {
      basicMaterial: 150,
    }
  },
  {
    name: '20mm',
    displayName: '20mm',
    icon: 'icons/ItemIcons/ATRifleAmmoItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'antiTankKinetic',
    crate: 10,
    team: 'warden',
    factory: true,
    cost: {
      basicMaterial: 150,
    }
  },
  {
    name: '120mm',
    displayName: '120mm',
    icon: 'icons/ItemIcons/LightArtilleryAmmoIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'highExplosive',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 60,
      explosiveMaterial: 15,
      time: 55
    }
  },
  {
    name: '150mm',
    displayName: '150mm',
    icon: 'icons/ItemIcons/HeavyArtilleryAmmoItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'highExplosive',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 120,
      heavyExplosiveMaterial: 10,
      time: 65
    }
  },
  {
    name: 'a3HarpaFragmentationGrenade',
    displayName: 'A3 Harpa Fragmentation Grenade',
    icon: 'icons/ItemIcons/GrenadeItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'lightKinetic',
    crate: 20,
    team: 'warden',
    factory: true,
    cost: {
      basicMaterial: 100,
      explosiveMaterial: 20,
      time: 100
    }
  },
  {
    name: '40mmRound',
    displayName: '40mm Round',
    icon: 'icons/ItemIcons/LightTankAmmoItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'explosive',
    crate: 20,
    factory: true,
    cost: {
      basicMaterial: 160,
      explosiveMaterial: 120,
      time: 200
    }
  },
  {
    name: '68mmAT',
    displayName: '68mm AT',
    icon: 'icons/ItemIcons/ATAmmoIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'armourPiercing',
    crate: 20,
    factory: true,
    cost: {
      basicMaterial: 120,
      explosiveMaterial: 120,
      time: 200
    }
  },
  {
    name: '250mm',
    displayName: '250mm',
    icon: 'icons/ItemIcons/ATAmmoIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'demolition',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 120,
      heavyExplosiveMaterial: 25,
      time: 150
    }
  },
  {
    name: 'warHead',
    displayName: 'Warhead',
    icon: 'icons/ItemIcons/RocketWarheadIcon.png',
    type: 'heavyArms',
    crate: 1,
    factory: true,
    cost: {
      refinedMaterial: 200,
      heavyExplosiveMaterial: 1000,
      time: 600
    }
  },
  {
    name: '300mmRound',
    displayName: '300mm Round',
    icon: 'icons/ItemIcons/LRArtilleryAmmoItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'demolition',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 135,
      heavyExplosiveMaterial: 30,
      time: 125
    }
  },
  {
    name: '30mm',
    displayName: '30mm',
    icon: 'icons/ItemIcons/MiniTankAmmoItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    damageType: 'explosive',
    crate: 20,
    factory: true,
    cost: {
      basicMaterial: 40,
      explosiveMaterial: 20,
      time: 100
    }
  },
  {
    name: 'maloneMK2',
    displayName: 'Malone MK.2',
    icon: 'icons/ItemIcons/MGWItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 10,
    team: 'warden',
    ammo: ['12dot7mm'],
    factory: true,
    cost: {
      refinedMaterial: 30,
    }
  },
  {
    name: 'atrpgIndirectShell',
    displayName: 'A.T.R.P.G. Indirect Shell',
    icon: 'icons/ItemIcons/ATRpgAmmoItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 15,
    damageType: 'armourPiercing',
    factory: true,
    cost: {
      basicMaterial: 60,
      explosiveMaterial: 75,
      time: 112
    }
  },
  {
    name: 'bonesawMK3',
    displayName: 'Bonesaw MK.3',
    icon: 'icons/ItemIcons/ATMortarItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 5,
    damageType: 'armourPiercing',
    team: 'warden',
    ammo: ['atrpgIndirectShell'],
    factory: true,
    cost: {
      basicMaterial: 100,
      refinedMaterial: 25,
    }
  },
  {
    name: 'mountedBonesawMK3',
    displayName: 'Mounted Bonesaw MK.3',
    icon: 'icons/ItemIcons/ATMortarWTripodItemIcon.png',
    type: 'heavyArms',
    massProductionFactory: true,
    crate: 5,
    team: 'warden',
    damageType: 'armourPiercing',
    ammo: ['atrpgIndirectShell'],
    factory: true,
    cost: {
      200: 100
    }
  },
  {
    name: 'krr3792Auger',
    displayName: 'KRR3-792 Auger',
    icon: 'icons/ItemIcons/SniperRifleCItemIcon.png',
    type: 'smallArms',
    massProductionFactory: true,
    team: 'colonial',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 200,
      refinedMaterial: 15
    }
  },
  /**
   * Medicine
   */
  {
    name: 'bandages',
    displayName: 'Bandages',
    icon: 'icons/ItemIcons/BandagesItemIcon.png',
    type: 'medical',
    crate: 80,
    factory: true,
    cost: {
      basicMaterial: 160,
      time: 100
    }
  },
  {
    name: 'bloodPlasma',
    displayName: 'Blood Plasma',
    icon: 'icons/ItemIcons/BloodPlasmaItemIcon.png',
    type: 'medical',
    crate: 80,
    factory: true,
    cost: {
      basicMaterial: 80,
      time: 40
    }
  },
  {
    name: 'firstAirdKit',
    displayName: 'First Aid Kit',
    icon: 'icons/ItemIcons/FirstAidKitItem.png',
    type: 'medical',
    crate: 10,
    ammo: ['bandages'],
    factory: true,
    cost: {
      basicMaterial: 60,
      time: 35
    }
  },
  {
    name: 'traumaKit',
    displayName: 'Trauma Kit',
    icon: 'icons/ItemIcons/TraumaKitItemIcon.png',
    type: 'medical',
    crate: 10,
    ammo: ['bloodPlasma'],
    factory: true,
    cost: {
      basicMaterial: 80,
      time: 50
    }
  },

  /**
   * Supplies
   */
  {
    name: 'bunkerSupplies',
    displayName: 'Bunker Supplies',
    icon: 'icons/ItemIcons/BunkerSuppliesIcon.png',
    type: 'supplies',
    crate: 150,
    factory: true,
    cost: {
      basicMaterial: 75,
      time: 225
    }
  },
  {
    name: 'garrisonSupplies',
    displayName: 'Garrison Supplies',
    icon: 'icons/ItemIcons/GarrisonSuppliesIcon.png',
    type: 'supplies',
    crate: 150,
    factory: true,
    cost: {
      basicMaterial: 75,
      time: 225
    }
  },
  {
    name: 'soldierSupplies',
    displayName: 'Soldier Supplies',
    icon: 'icons/ItemIcons/ClothItemIcon.png',
    type: 'supplies',
    crate: 10,
    factory: true,
    cost: {
      basicMaterial: 80,
      time: 80
    }
  },

  /**
   * Utility
   */
  {
    name: 'binoculars',
    displayName: 'Binoculars',
    icon: 'icons/ItemIcons/BinocularsItemIcon.png',
    type: 'utility',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 75,
      time: 50
    }
  },
  {
    name: 'gasMask',
    displayName: 'Gas Mask',
    icon: 'icons/ItemIcons/GasmaskIcon.png',
    type: 'utility',
    crate: 20,
    ammo: ['gasMaskFilter'],                     // remove line if no ammo
    factory: true,
    cost: {
      basicMaterial: 160,
      time: 100
    }
  },
  {
    name: 'gasMaskFilter',
    displayName: 'Gas Mask Filter',
    icon: 'icons/ItemIcons/GasMaskFilterIcon.png',
    type: 'utility',
    crate: 20,
    factory: true,
    cost: {
      basicMaterial: 100,
      time: 50
    }
  },
  {
    name: 'wrench',
    displayName: 'Wrench',
    icon: 'icons/ItemIcons/WorkWrench.png',
    type: 'utility',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 75,
      time: 50
    }
  },
  {
    name: 'radio',
    displayName: 'Radio',
    icon: 'icons/ItemIcons/RadioItemIcon.png',
    type: 'utility',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 100,
      time: 50
    }
  },
  {
    name: 'radioBackpack',
    displayName: 'Radio Backpack',
    icon: 'icons/ItemIcons/RadioBackpackItemIcon.png',
    type: 'utility',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 150,
      time: 75
    }
  },
  {
    name: 'buckhornCCQ18',
    displayName: 'Buckhorn CCQ-18',
    icon: 'icons/ItemIcons/BayonetIcon.png',
    type: 'utility',
    crate: 20,
    factory: true,
    cost: {
      basicMaterial: 40,
      time: 30
    }
  },
  {
    name: 'alligatorCharge',
    displayName: 'Alligator Charge',
    icon: 'icons/StructureIcons/SatchelCharge.png',
    type: 'utility',
    crate: 5,
    factory: true,
    cost: {
      basicMaterial: 100,
      heavyExplosiveMaterial: 15,
    }
  },
  {
    name: 'sledgeHammer',
    displayName: 'Sledge Hammer',
    icon: 'icons/ItemIcons/SledgeHammerItemIcon.png',
    type: 'utility',
    crate: 10,
    factory: true,
    cost: {
      basicMaterial: 200,
      time: 100
    }
  },
  {
    name: 'abismeAT99',
    displayName: 'Abisme AT-99',
    icon: 'icons/ItemIcons/AntiTankMineItemIcon.png',
    type: 'utility',
    crate: 10,
    factory: true,
    cost: {
      basicMaterial: 100,
      explosiveMaterial: 10,
      time: 100
    }
  },
  {
    name: 'rocketBooster',
    displayName: 'Rocket Booster',
    icon: 'icons/ItemIcons/RocketBoosterIcon.png',
    type: 'utility',
    crate: 1,
    factory: true,
    cost: {
      refinedMaterial: 800,
    }
  },
  {
    name: 'shovel',
    displayName: 'Shovel',
    icon: 'icons/ItemIcons/ShovelIcon.png',
    type: 'utility',
    crate: 10,
    factory: true,
    cost: {
      basicMaterial: 200,
      time: 100
    }
  },
  {
    name: 'tripod',
    displayName: 'Tripod',
    icon: 'icons/ItemIcons/DeployableTripodItemIcon.png', // DeployableTripodItemIcon.png
    type: 'utility',
    crate: 10,
    factory: true,
    cost: {
      basicMaterial: 200,
      time: 100
    }
  },

  /**
   * Vehicles
   */
  {
    name: 'duneResponder3e',
    displayName: 'Dune Responder 3e',
    icon: 'icons/VehicleIcons/AmbulanceWar.png',
    type: 'ambulance',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'warden',
    garage: true,
    cost: {
      basicMaterial: 150
    }
  },
  {
    name: 'oBrianv121Highlander',
    displayName: 'O\'Brian v.121 Highlander',
    icon: 'icons/VehicleIcons/ArmoredCarMobilityWarVehicleIcon.png',
    type: 'armouredCar',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    inventory: {
      slots: 2
    },
    ammo: ['7dot92mm'],
    team: 'warden',
    garage: true,
    cost: {
      refinedMaterial: 40
    }
  },
  {
    name: 'oBrianv101Freeman',
    displayName: 'O\'Brian v.101 Freeman',
    icon: 'icons/VehicleIcons/ArmoredCarOffensiveWVehicleIcon.png',
    type: 'armouredCar',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    inventory: {
      slots: 1
    },
    ammo: ['40mm'],
    team: 'warden',
    garage: true,
    cost: {
      refinedMaterial: 60
    }
  },
  {
    name: 'oBrianv110',
    displayName: 'O\'Brian v.110',
    icon: 'icons/VehicleIcons/ArmoredCarWarVehicleIcon.png',
    type: 'armouredCar',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    inventory: {
      slots: 1
    },
    ammo: ['7dot92mm'],
    team: 'warden',
    garage: true,
    cost: {
      refinedMaterial: 45
    }
  },
  {
    name: 'dunneCaravaner2f',
    displayName: 'Dunne Caravaner 2f',
    icon: 'icons/VehicleIcons/BusWarIcon.png',
    type: 'transportBus',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 11,
    inventory: {
      slots: 1
    },
    team: 'warden',
    garage: true,
    cost: {
      basicMaterial: 100
    }
  },
  {
    name: 'collinsCanon68mm',
    displayName: 'Collins Canon 68mm',
    icon: 'icons/VehicleIcons/FieldAntiTankWarVehicleIcon.png',
    type: 'fieldATGun',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    ammo: ['68mmAT'],
    inventory: {
      slots: 1
    },
    team: 'warden',
    garage: true,
    cost: {
      refinedMaterial: 30
    }
  },
  {
    name: 'balfourWolfhound40mm',
    displayName: 'Balfour Wolfhound 40mm',
    icon: 'icons/VehicleIcons/FieldCannonWVehicleIcon.png',
    type: 'fieldCanon',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    ammo: ['40mm'],
    inventory: {
      slots: 1
    },
    team: 'warden',
    garage: true,
    cost: {
      refinedMaterial: 30
    }
  },
  {
    name: 'swallowtail9881452',
    displayName: 'Swallowtail 988/145-2',
    icon: 'icons/VehicleIcons/FieldMachineGunWar.png',
    type: 'fieldMachineGun',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    ammo: ['12dot7mm'],
    inventory: {
      slots: 1
    },
    team: 'warden',
    garage: true,
    cost: {
      refinedMaterial: 25
    }
  },
  {
    name: 'balfourFalconer250mm',
    displayName: 'Balfour Falconer 250mm',
    icon: 'icons/VehicleIcons/FieldMortarWIcon.png',
    type: 'fieldMortar',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    ammo: ['250mm'],
    inventory: {
      slots: 6
    },
    team: 'warden',
    garage: true,
    cost: {
      refinedMaterial: 35
    }
  },
  {
    name: 'bmsPackmuleFlatbed',
    displayName: 'BMS - Packmule Flatbed',
    icon: 'icons/VehicleIcons/FlatbedTruckVehicleIcon.png',
    type: 'flatbedTruck',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    inventory: {
      bigSlots: 1
    },
    garage: true,
    cost: {
      refinedMaterial: 30
    }
  },
  {
    name: 'niskaMK2Blinder',
    displayName: 'Niska Mk.2 Blinder',
    icon: 'icons/VehicleIcons/HalfTrackOffensiveWarVehicleIcon.png',
    type: 'halfTrack',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'warden',
    ammo: ['68mmAT'],
    inventory: {
      slots: 3
    },
    garage: true,
    cost: {
      refinedMaterial: 85
    }
  },
  {
    name: 'niskaMk1GunMotorCarriage',
    displayName: 'Niska MK.1 Gun Motor Carriage',
    icon: 'icons/VehicleIcons/HalfTrackWarVehicleIcon.png',
    type: 'halfTrack',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 3,
    ammo: ['12dot7mm'],
    inventory: {
      slots: 3
    },
    garage: true,
    cost: {
      refinedMaterial: 60
    }
  },
  {
    name: 'bmsScrapHauler',
    displayName: 'BMS - Scrap Hauler',
    icon: 'icons/VehicleIcons/Harvester.png',
    type: 'harvester',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 1,
    inventory: {
      slots: 15
    },
    garage: true,
    cost: {
      refinedMaterial: 30
    }
  },
  {
    name: 'mulloyLPC',
    displayName: 'Mulloy LPC',
    icon: 'icons/VehicleIcons/LandingCraftWarVehicleIcon.png',
    type: 'landingAPC',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 10,
    team: 'warden',
    inventory: {
      slots: 6
    },
    garage: true,
    cost: {
      refinedMaterial: 20
    }
  },
  {
    name: 'devittCaineMKIVMMR',
    displayName: 'Devitt-Caine MK-IV MMR',
    icon: 'icons/VehicleIcons/LightTankArtilleryWar.png',
    type: 'lightTank',
    massProductionFactory: true,
    bigCrate: 3,
    ammo: ['mortarShell', 'mortarShellShrapnel', 'mortarShellFlare'],
    crew: 3,
    team: 'warden',
    inventory: {
      slots: 14
    },
    garage: true,
    cost: {
      refinedMaterial: 180
    }
  },
  {
    name: 'devittIronhideMKIV',
    displayName: 'Devitt Ironhide Mk.IV',
    icon: 'icons/VehicleIcons/LightTankWarDefensiveVehicleIcon.png',
    type: 'lightTank',
    massProductionFactory: true,
    bigCrate: 3,
    ammo: ['40mm'],
    crew: 3,
    team: 'warden',
    inventory: {
      slots: 1
    },
    garage: true,
    cost: {
      refinedMaterial: 160
    }
  },
  {
    name: 'devitMkIII',
    displayName: 'Devitt Mk. III',
    icon: 'icons/VehicleIcons/LightTankWarVehicleIcon.png',
    type: 'lightTank',
    massProductionFactory: true,
    bigCrate: 3,
    ammo: ['40mm'],
    crew: 3,
    team: 'warden',
    inventory: {
      slots: 1
    },
    garage: true,
    cost: {
      refinedMaterial: 140
    }
  },
  {
    name: 'silverHandMKIV',
    displayName: 'Silverhand - Mk. IV',
    icon: 'icons/VehicleIcons/WardenMediumTankIcon.png',
    type: 'assaultTank',
    massProductionFactory: true,
    bigCrate: 3,
    ammo: ['40mm', '68mmAT'],
    crew: 4,
    team: 'warden',
    inventory: {
      slots: 3
    },
    garage: true,
    cost: {
      refinedMaterial: 170
    }
  },
  {
    name: 'kivelaPowerWheel801',
    displayName: 'Kivela Power Wheel 80-1',
    icon: 'icons/VehicleIcons/MotorcycleVehicleIcon.png',
    type: 'motorcycle',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'warden',
    inventory: {
      slots: 2
    },
    garage: true,
    cost: {
      basicMaterial: 85
    }
  },
  {
    name: 'dunneFuelrunner2d',
    displayName: 'Dunne Fuelrunner 2d',
    icon: 'icons/VehicleIcons/OilTankerWarIcon.png',
    type: 'fuelTanker',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'warden',
    inventory: {
      slots: 2
    },
    garage: true,
    cost: {
      basicMaterial: 100
    }
  },
  {
    name: 'dunneTransport',
    displayName: 'Dunne Transport',
    icon: 'icons/VehicleIcons/TruckWarVehicleIcon.png',
    type: 'truck',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 6,
    team: 'warden',
    inventory: {
      slots: 15
    },
    garage: true,
    cost: {
      basicMaterial: 100
    }
  },
  {
    name: 'dunneLandrunner12c',
    displayName: 'Dunne Landrunner 12c',
    icon: 'icons/VehicleIcons/TruckMobilityWarVehicleIcon.png',
    type: 'truck',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 5,
    team: 'warden',
    inventory: {
      slots: 14
    },
    garage: true,
    cost: {
      basicMaterial: 120
    }
  },
  {
    name: 'dunneLoadLugger3c',
    displayName: 'Dunne Loadlugger 3c',
    icon: 'icons/VehicleIcons/TruckWarVehicleIcon.png',
    type: 'truck',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'warden',
    inventory: {
      slots: 20
    },
    garage: true,
    cost: {
      basicMaterial: 120
    }
  },
  {
    name: 'dunneLeatherback2a',
    displayName: 'Dunne Leatherback 2a',
    icon: 'icons/VehicleIcons/TruckDefensiveWIcon.png',
    type: 'truck',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 6,
    team: 'warden',
    inventory: {
      slots: 14
    },
    garage: true,
    cost: {
      basicMaterial: 145
    }
  },
  {
    name: 'drummond100a',
    displayName: 'Drummond 100a',
    icon: 'icons/VehicleIcons/ScoutVehicleWarVehicleIcon.png',
    type: 'lightUtilityVehicle',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'warden',
    inventory: {
      slots: 9
    },
    garage: true,
    cost: {
      refinedMaterial: 10
    }
  },
  {
    name: 'kingSpireMKI',
    displayName: 'King Spire Mk-I',
    icon: 'icons/VehicleIcons/ScoutVehicleWar.png',
    type: 'scoutTank',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 3,
    team: 'warden',
    inventory: {
      slots: 3
    },
    garage: true,
    cost: {
      refinedMaterial: 80
    }
  },
  {
    name: 'drummondSpitfire100d',
    displayName: 'Drummond Spitfire 100d',
    icon: 'icons/VehicleIcons/ScoutVehicleOffensiveWarVehicleIcon.png',
    type: 'lightUtilityVehicle',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'warden',
    inventory: {
      slots: 3
    },
    garage: true,
    cost: {
      refinedMaterial: 15
    }
  },
  {
    name: 'drummondLoscann55c',
    displayName: 'Drummond Loscann 55c',
    icon: 'icons/VehicleIcons/ScoutVehicleAmphibiousWarVehicleIcon.png',
    type: 'lightUtilityVehicle',
    massProductionFactory: true,
    crew: 3,
    team: 'warden',
    inventory: {
      slots: 7
    },
    garage: true,
    cost: {
      refinedMaterial: 10
    }
  },
  {
    name: 'koronidesFieldGun',
    displayName: '120-68 "Koronides" Field Gun',
    icon: 'icons/VehicleIcons/FieldArtilleryColVehicleIcon.png',
    type: 'lightArtillery',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 2,
    team: 'colonial',
    garage: true,
    cost: {
      refinedMaterial: 50
    }
  },
  /**
   * Boats
   */
  {
    name: 'bmsAquatipper',
    displayName: 'BMS - Aquatipper',
    icon: 'icons/VehicleIcons/BargeVehicleIcon.png',
    type: 'barge',
    massProductionFactory: true,
    bigCrate: 3,
    crew: 7,
    inventory: {
      slots: 15
    },
    shipyard: true,
    cost: {
      basicMaterial: 150
    }
  },
  {
    name: 'bmsIronship',
    displayName: 'BMS - Ironship',
    icon: 'icons/VehicleIcons/FreighterVehicleIcon.png',
    type: 'freighter',
    massProductionFactory:{},
    bigCrate: 3,
    crew: 2,
    inventory: {
      slots: 10,
      bigSlots: 5
    },
    shipyard: true,
    cost: {
      basicMaterial: 500
    }
  },
  {
    name: '74c2RonanMeteoraGunship',
    displayName: '74c-2 Ronan Meteora Gunship',
    icon: 'icons/VehicleIcons/GunboatWarDoubleArtilleryVehicleIcon.png',
    type: 'gunboat',
    massProductionFactory:{},
    bigCrate: 3,
    crew: 4,
    team: 'warden',
    ammo: ['120mm'],
    inventory: {
      slots: 30
    },
    shipyard: true,
    cost: {
      refinedMaterial: 160
    }
  },
  {
    name: '74b1RonanGunship',
    displayName: '74b-1 Ronan Gunship',
    icon: 'icons/VehicleIcons/GunboatWarVehicleIcon.png',
    type: 'gunboat',
    massProductionFactory:{},
    bigCrate: 3,
    crew: 4,
    team: 'warden',
    ammo: ['120mm', '12dot7mm'],
    inventory: {
      slots: 30
    },
    shipyard: true,
    cost: {
      refinedMaterial: 160
    }
  },
  {
    name: 'TypeCCharon',
    displayName: 'Type C - "Charon"',
    icon: 'icons/VehicleIcons/GunboatVehicleIcon.png',
    type: 'gunboat',
    massProductionFactory:{},
    bigCrate: 3,
    crew: 4,
    team: 'colonial',
    ammo: ['120mm', '12dot7mm'],
    inventory: {
      slots: 30
    },
    shipyard: true,
    cost: {
      refinedMaterial: 160
    }
  },
  {
    name: 'bmsWhiteWhale',
    displayName: 'BMS - White Whale',
    icon: 'icons/VehicleIcons/TroopShipVehicleIcon.png',
    type: 'landingShip',
    massProductionFactory:{},
    bigCrate: 3,
    crew: 1,
    inventory: {
      slots: 12
    },
    shipyard: true,
    cost: {
      refinedMaterial: 100
    }
  },
  {
    name: 'bmsUniversalAssemblyRig',
    displayName: 'BMS - Universal Assembly Rig',
    icon: 'icons/VehicleIcons/ConstructionVehicleIcon.png',
    type: 'constructionVehicle',
    massProductionFactory:{},
    bigCrate: 3,
    crew: 1,
    inventory: {
      slots: 12
    },
    shipyard: true,
    cost: {
      basicMaterial: 100
    }
  },
  {
    name: 'bmsclass2MobileAutoCrane',
    displayName: 'BMS - Class 2 Mobile Auto-Crane',
    icon: 'icons/VehicleIcons/CraneVehicleIcon.png',
    type: 'crane',
    massProductionFactory:{},
    bigCrate: 3,
    crew: 1,
    inventory: {
      slots: 12
    },
    shipyard: true,
    cost: {
      basicMaterial: 100
    }
  },
  /**
   * staticItems
   */
  {
    name: 'concreteMixer',
    displayName: 'Concrete Mixer',
    icon: 'icons/VehicleIcons/ConcreteMixerIcon.png',
    type: 'concreteMixer',
    bigCrate: 3,
    massProductionfactory: true,
    cost: {
      refinedMaterial: 75
    }
  },
  {
    name: '68mmAntiTankCanon',
    displayName: '68mm Anti-Tank Canon',
    icon: 'icons/VehicleIcons/EmplacedATIcon.png',
    type: 'antiTankEmplacement',
    ammo: ['68mm'],
    bigCrate: 3,
    crew: 2,
    massProductionfactory: true,
    cost: {
      basicMaterial: 150
    }
  },
  {
    name: 'hubertExalt150mm',
    displayName: 'Hubert Exalt 150mm',
    icon: 'icons/VehicleIcons/EmplacedHowitzerIcon.png',
    type: 'artilleryEmplacement',
    ammo: ['150mm'],
    crew: 2,
    team: 'warden',
    bigCrate: 3,
    massProductionfactory: true,
    cost: {
      refinedMaterial: 175
    }
  },
  {
    name: 'hubertLariat120mm',
    displayName: 'Hubert Lariat 120mm',
    icon: 'icons/VehicleIcons/HeavyArtilleryW.png',
    type: 'artilleryEmplacement',
    ammo: ['120mm'],
    crew: 2,
    team: 'warden',
    bigCrate: 3,
    massProductionfactory: true,
    cost: {
      refinedMaterial: 35
    }
  }
]
